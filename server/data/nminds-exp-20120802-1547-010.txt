subblock_no	block_no	practice	TrialNumber	target_pos	target_screen_nr	contrast_idx	ran	contrast	group_dec_maker	group_dec	group_dec_rt	participants	feedback_texts_order	0_indiv_dec	0_indiv_dec_rt	0_client	1_indiv_dec	1_indiv_dec_rt	1_client	2_indiv_dec	2_indiv_dec_rt	2_client
0	0	True	1	0	0	1	1.0	0.115	0.0	1.0	34.7812	[0, 1, 2]	[0, 1, 2]	1.0	7.81339	client-A	1.0	4.12689	client-B	0.0	2.45305	client-C
0	0	True	2	4	1	3	1.0	0.17	1.0	0.0	24.2219	[0, 1, 2]	[0, 1, 2]	1.0	22.2094	client-A	0.0	49.8507	client-B	0.0	26.5463	client-C
0	0	True	3	3	0	2	1.0	0.135	2.0	1.0	15.8977	[0, 1, 2]	[0, 1, 2]	0.0	15.7537	client-A	1.0	16.0541	client-B	1.0	1.21751	client-C
0	0	True	4	2	1	2	1.0	0.135	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	23.6774	client-A	0.0	1.75185	client-B	0.0	24.311	client-C
0	0	True	5	1	1	4	1.0	0.25	0.0	1.0	12.645	[0, 1, 2]	[0, 1, 2]	1.0	2.08635	client-A	0.0	1.23472	client-B	1.0	0.617484	client-C
0	0	True	6	0	0	3	1.0	0.17	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	7.90732	client-A	0.0	5.73873	client-B	0.0	2.90293	client-C
0	0	True	7	5	1	1	1.0	0.115	1.0	1.0	10.7656	[0, 1, 2]	[0, 1, 2]	1.0	2.15223	client-A	0.0	1.85196	client-B	1.0	0.70092	client-C
0	0	True	8	0	0	4	1.0	0.25	2.0	0.0	6.42276	[0, 1, 2]	[0, 1, 2]	1.0	2.65265	client-A	0.0	2.68567	client-B	0.0	1.10127	client-C
0	0	False	1	4	1	4	1.0	0.25	0.0	1.0	18.2561	[0, 1, 2]	[0, 1, 2]	1.0	1.18468	client-A	1.0	2.96957	client-B	0.0	0.684196	client-C
0	0	False	2	4	1	3	1.0	0.17	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	1.41826	client-A	1.0	1.90197	client-B	1.0	0.767658	client-C
0	0	False	3	3	0	4	1.0	0.25	1.0	0.0	4.83813	[0, 1, 2]	[0, 1, 2]	1.0	0.934487	client-A	0.0	1.10126	client-B	0.0	0.383982	client-C
0	0	False	4	4	1	3	1.0	0.17	2.0	1.0	10.2934	[0, 1, 2]	[0, 1, 2]	1.0	1.00125	client-A	0.0	1.21806	client-B	1.0	0.784298	client-C
0	0	False	5	2	1	2	1.0	0.135	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	1.08464	client-A	1.0	3.48673	client-B	1.0	1.08459	client-C
0	0	False	6	3	0	1	1.0	0.115	0.0	0.0	10.9936	[0, 1, 2]	[0, 1, 2]	1.0	2.76944	client-A	0.0	1.76855	client-B	0.0	0.617497	client-C
0	0	False	7	3	0	4	1.0	0.25	1.0	0.0	3.82053	[0, 1, 2]	[0, 1, 2]	1.0	1.10131	client-A	0.0	0.984527	client-B	0.0	2.30224	client-C
0	0	False	8	4	1	4	1.0	0.25	2.0	1.0	9.58164	[0, 1, 2]	[0, 1, 2]	1.0	1.7519	client-A	0.0	1.83528	client-B	1.0	0.750978	client-C
0	0	False	9	4	1	4	1.0	0.25	0.0	1.0	16.832	[0, 1, 2]	[0, 1, 2]	1.0	1.51833	client-A	0.0	5.4051	client-B	0.0	0.667579	client-C
0	0	False	10	4	0	3	1.0	0.17	1.0	0.0	41.6263	[0, 1, 2]	[0, 1, 2]	1.0	3.41999	client-A	0.0	5.77209	client-B	1.0	9.09159	client-C
0	0	False	11	3	0	4	1.0	0.25	2.0	0.0	5.72832	[0, 1, 2]	[0, 1, 2]	1.0	1.73517	client-A	0.0	1.10128	client-B	0.0	0.51739	client-C
0	0	False	12	4	1	3	1.0	0.17	0.0	1.0	5.0549	[0, 1, 2]	[0, 1, 2]	1.0	1.71855	client-A	0.0	3.00299	client-B	1.0	0.584162	client-C
0	0	False	13	4	1	4	1.0	0.25	1.0	1.0	13.7961	[0, 1, 2]	[0, 1, 2]	0.0	1.93538	client-A	0.0	1.76851	client-B	1.0	0.58415	client-C
0	0	False	14	5	0	2	1.0	0.135	2.0	0.0	17.0882	[0, 1, 2]	[0, 1, 2]	1.0	1.0012	client-A	0.0	0.934449	client-B	1.0	0.700927	client-C
0	0	False	15	4	0	3	1.0	0.17	0.0	0.0	3.53695	[0, 1, 2]	[0, 1, 2]	1.0	3.37001	client-A	0.0	1.28478	client-B	0.0	3.33656	client-C
0	0	False	16	2	1	2	1.0	0.135	1.0	1.0	5.77227	[0, 1, 2]	[0, 1, 2]	1.0	2.8028	client-A	0.0	1.91867	client-B	1.0	1.43488	client-C
0	0	False	17	3	0	1	1.0	0.115	2.0	0.0	9.71521	[0, 1, 2]	[0, 1, 2]	0.0	1.18471	client-A	1.0	1.33481	client-B	0.0	1.70179	client-C
0	0	False	18	0	1	1	1.0	0.115	0.0	0.0	6.83988	[0, 1, 2]	[0, 1, 2]	0.0	5.17782	client-A	0.0	6.68956	client-B	1.0	0.617502	client-C
0	0	False	19	5	0	2	1.0	0.135	1.0	1.0	2.91975	[0, 1, 2]	[0, 1, 2]	1.0	2.35238	client-A	0.0	1.78523	client-B	1.0	1.03454	client-C
0	0	False	20	4	0	3	1.0	0.17	2.0	0.0	3.48683	[0, 1, 2]	[0, 1, 2]	1.0	1.06794	client-A	0.0	1.18469	client-B	0.0	0.484035	client-C
0	0	False	21	3	0	1	1.0	0.115	0.0	1.0	5.23846	[0, 1, 2]	[0, 1, 2]	1.0	1.40157	client-A	0.0	3.04191	client-B	1.0	2.50307	client-C
0	0	False	22	0	1	1	1.0	0.115	1.0	0.0	11.1437	[0, 1, 2]	[0, 1, 2]	1.0	1.70183	client-A	0.0	0.917763	client-B	1.0	0.833047	client-C
0	0	False	23	5	0	2	1.0	0.135	2.0	1.0	15.1533	[0, 1, 2]	[0, 1, 2]	1.0	3.48673	client-A	1.0	6.35596	client-B	0.0	8.30755	client-C
0	0	False	24	4	0	3	1.0	0.17	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	5.86172	client-A	0.0	6.43937	client-B	0.0	6.08897	client-C
0	0	False	25	2	1	2	1.0	0.135	0.0	0.0	4.15415	[0, 1, 2]	[0, 1, 2]	1.0	1.15221	client-A	0.0	2.55259	client-B	0.0	0.75096	client-C
0	0	False	26	3	0	1	1.0	0.115	1.0	1.0	5.83902	[0, 1, 2]	[0, 1, 2]	1.0	0.851081	client-A	0.0	1.6184	client-B	1.0	1.38486	client-C
0	0	False	27	3	0	4	1.0	0.25	2.0	0.0	10.8161	[0, 1, 2]	[0, 1, 2]	1.0	3.82036	client-A	0.0	1.05125	client-B	0.0	0.333921	client-C
0	0	False	28	2	1	2	1.0	0.135	0.0	1.0	1.91897	[0, 1, 2]	[0, 1, 2]	1.0	1.25144	client-A	1.0	1.20104	client-B	0.0	0.951092	client-C
0	0	False	29	4	1	3	1.0	0.17	1.0	0.0	6.63968	[0, 1, 2]	[0, 1, 2]	0.0	1.85196	client-A	1.0	1.40154	client-B	0.0	0.717606	client-C
0	0	False	30	0	1	1	1.0	0.115	2.0	0.0	5.40518	[0, 1, 2]	[0, 1, 2]	0.0	2.01878	client-A	1.0	1.85193	client-B	0.0	0.784334	client-C
0	0	False	31	5	0	2	1.0	0.135	0.0	0.0	2.55272	[0, 1, 2]	[0, 1, 2]	1.0	1.58506	client-A	1.0	5.42734	client-B	0.0	0.968416	client-C
0	0	False	32	0	1	1	1.0	0.115	1.0	1.0	11.6275	[0, 1, 2]	[0, 1, 2]	1.0	1.31879	client-A	0.0	1.68514	client-B	1.0	0.934419	client-C
1	0	False	1	0	1	2	1.0	0.135	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	2.08547	client-A	1.0	4.02049	client-B	1.0	0.917783	client-C
1	0	False	2	5	0	2	1.0	0.135	2.0	0.0	4.33757	[0, 1, 2]	[0, 1, 2]	0.0	1.46829	client-A	1.0	1.10128	client-B	0.0	0.40063	client-C
1	0	False	3	0	1	1	1.0	0.115	0.0	0.0	1.50183	[0, 1, 2]	[0, 1, 2]	1.0	1.16803	client-A	0.0	1.81858	client-B	0.0	0.550805	client-C
1	0	False	4	1	0	1	1.0	0.115	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	1.78519	client-A	1.0	1.11795	client-B	1.0	2.28559	client-C
1	0	False	5	1	0	1	1.0	0.115	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	5.87771	client-A	0.0	1.13463	client-B	0.0	3.10299	client-C
1	0	False	6	0	1	1	1.0	0.115	1.0	0.0	3.90395	[0, 1, 2]	[0, 1, 2]	0.0	1.18469	client-A	0.0	0.951117	client-B	1.0	0.83439	client-C
1	0	False	7	5	1	3	1.0	0.17	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	1.33483	client-A	1.0	1.03455	client-B	1.0	0.817717	client-C
1	0	False	8	4	1	4	1.0	0.25	2.0	1.0	0.784469	[0, 1, 2]	[0, 1, 2]	0.0	1.61843	client-A	1.0	1.05126	client-B	1.0	0.63415	client-C
1	0	False	9	0	1	2	1.0	0.135	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	0.85108	client-A	1.0	0.91778	client-B	1.0	0.934441	client-C
1	0	False	10	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	0.70096	client-A	0.0	0.867735	client-B	0.0	0.200522	client-C
1	0	False	11	1	0	3	1.0	0.17	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	1.05126	client-A	0.0	0.750932	client-B	0.0	0.584117	client-C
1	0	False	12	1	0	3	1.0	0.17	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	0.801031	client-A	1.0	0.801	client-B	1.0	0.750457	client-C
1	0	False	13	0	1	1	1.0	0.115	0.0	1.0	1.57413	[0, 1, 2]	[0, 1, 2]	1.0	0.884453	client-A	0.0	0.634187	client-B	1.0	0.884393	client-C
1	0	False	14	0	1	1	1.0	0.115	1.0	1.0	2.81962	[0, 1, 2]	[0, 1, 2]	1.0	1.31815	client-A	1.0	0.801008	client-B	0.0	1.03456	client-C
1	0	False	15	5	0	2	1.0	0.135	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	4.28744	client-A	1.0	2.20227	client-B	1.0	2.83612	client-C
1	0	False	16	5	1	3	1.0	0.17	2.0	0.0	2.9697	[0, 1, 2]	[0, 1, 2]	1.0	0.784339	client-A	0.0	1.0846	client-B	0.0	0.984484	client-C
1	0	False	17	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	1.08462	client-A	0.0	1.2347	client-B	0.0	0.300569	client-C
1	0	False	18	1	0	1	1.0	0.115	0.0	1.0	2.08566	[0, 1, 2]	[0, 1, 2]	1.0	1.00121	client-A	1.0	1.06793	client-B	0.0	0.75097	client-C
1	0	False	19	1	0	1	1.0	0.115	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	0.81768	client-A	1.0	0.87812	client-B	1.0	1.00117	client-C
1	0	False	20	5	1	3	1.0	0.17	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	0.934492	client-A	1.0	0.650895	client-B	1.0	1.45158	client-C
1	0	False	21	1	0	3	1.0	0.17	1.0	0.0	2.41934	[0, 1, 2]	[0, 1, 2]	1.0	5.20437	client-A	0.0	1.36815	client-B	0.0	0.884416	client-C
1	0	False	22	4	1	4	1.0	0.25	2.0	1.0	2.4359	[0, 1, 2]	[0, 1, 2]	0.0	1.58507	client-A	1.0	1.16801	client-B	0.0	0.484035	client-C
1	0	False	23	1	0	3	1.0	0.17	0.0	0.0	1.91887	[0, 1, 2]	[0, 1, 2]	1.0	2.31904	client-A	0.0	1.73515	client-B	0.0	0.951152	client-C
1	0	False	24	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	0.63421	client-A	0.0	0.851223	client-B	0.0	0.283838	client-C
1	0	False	25	5	1	3	1.0	0.17	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	1.13466	client-A	0.0	1.21806	client-B	0.0	0.567449	client-C
1	0	False	26	5	0	2	1.0	0.135	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	1.53502	client-A	1.0	1.03454	client-B	1.0	12.8282	client-C
1	0	False	27	0	1	2	1.0	0.135	1.0	1.0	2.88638	[0, 1, 2]	[0, 1, 2]	1.0	1.01791	client-A	1.0	1.05121	client-B	0.0	0.750972	client-C
1	0	False	28	5	0	2	1.0	0.135	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	1.80192	client-A	1.0	0.9345	client-B	1.0	1.43492	client-C
1	0	False	29	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	1.25165	client-A	0.0	1.00122	client-B	0.0	0.36731	client-C
1	0	False	30	4	1	4	1.0	0.25	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	0.83442	client-A	1.0	1.00117	client-B	1.0	0.717595	client-C
1	0	False	31	0	1	2	1.0	0.135	2.0	0.0	3.80442	[0, 1, 2]	[0, 1, 2]	0.0	1.23475	client-A	0.0	0.96781	client-B	1.0	0.767601	client-C
1	0	False	32	4	1	4	1.0	0.25	0.0	1.0	2.23641	[0, 1, 2]	[0, 1, 2]	1.0	0.717617	client-A	1.0	0.917799	client-B	0.0	3.75359	client-C
2	0	False	1	2	0	1	1.0	0.115	1.0	1.0	2.13568	[0, 1, 2]	[0, 1, 2]	0.0	2.13552	client-A	1.0	0.967797	client-B	1.0	0.817654	client-C
2	0	False	2	2	1	3	1.0	0.17	2.0	1.0	2.45265	[0, 1, 2]	[0, 1, 2]	1.0	1.35152	client-A	1.0	1.01788	client-B	0.0	1.65177	client-C
2	0	False	3	2	1	3	1.0	0.17	0.0	1.0	1.91886	[0, 1, 2]	[0, 1, 2]	0.0	2.98629	client-A	1.0	1.11794	client-B	1.0	1.16799	client-C
2	0	False	4	4	1	1	1.0	0.115	1.0	1.0	12.034	[0, 1, 2]	[0, 1, 2]	1.0	2.51922	client-A	0.0	0.951132	client-B	1.0	0.800967	client-C
2	0	False	5	5	0	4	1.0	0.25	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	0.934745	client-A	0.0	0.867769	client-B	0.0	0.60088	client-C
2	0	False	6	0	1	2	1.0	0.135	2.0	0.0	6.02242	[0, 1, 2]	[0, 1, 2]	0.0	0.734289	client-A	1.0	0.734299	client-B	0.0	0.417307	client-C
2	0	False	7	5	0	3	1.0	0.17	0.0	1.0	11.7108	[0, 1, 2]	[0, 1, 2]	1.0	2.35241	client-A	0.0	2.73612	client-B	1.0	1.45159	client-C
2	0	False	8	5	1	4	1.0	0.25	1.0	1.0	2.25247	[0, 1, 2]	[0, 1, 2]	1.0	4.20403	client-A	1.0	3.70922	client-B	0.0	1.06791	client-C
2	0	False	9	5	0	4	1.0	0.25	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	0.684263	client-A	0.0	0.667564	client-B	0.0	0.300554	client-C
2	0	False	10	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	0.767668	client-A	1.0	0.767625	client-B	1.0	0.634207	client-C
2	0	False	11	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	0.650869	client-A	1.0	0.767634	client-B	1.0	0.634165	client-C
2	0	False	12	4	0	2	1.0	0.135	2.0	0.0	1.83539	[0, 1, 2]	[0, 1, 2]	1.0	1.38488	client-A	0.0	0.75096	client-B	0.0	0.717601	client-C
2	0	False	13	4	1	1	1.0	0.115	0.0	1.0	16.3482	[0, 1, 2]	[0, 1, 2]	0.0	1.46831	client-A	1.0	0.801027	client-B	0.0	0.617501	client-C
2	0	False	14	2	1	3	1.0	0.17	1.0	1.0	13.2789	[0, 1, 2]	[0, 1, 2]	0.0	0.667563	client-A	1.0	0.968691	client-B	0.0	0.884414	client-C
2	0	False	15	2	0	1	1.0	0.115	2.0	0.0	4.72128	[0, 1, 2]	[0, 1, 2]	1.0	1.30148	client-A	0.0	1.65179	client-B	0.0	0.650879	client-C
2	0	False	16	2	0	1	1.0	0.115	0.0	0.0	6.57295	[0, 1, 2]	[0, 1, 2]	1.0	2.23564	client-A	0.0	1.96874	client-B	1.0	2.18553	client-C
2	0	False	17	5	0	3	1.0	0.17	1.0	0.0	3.77051	[0, 1, 2]	[0, 1, 2]	1.0	0.484103	client-A	0.0	1.81855	client-B	0.0	1.10126	client-C
2	0	False	18	5	0	4	1.0	0.25	2.0	0.0	1.13476	[0, 1, 2]	[0, 1, 2]	1.0	0.784349	client-A	0.0	1.48493	client-B	0.0	0.350597	client-C
2	0	False	19	0	1	2	1.0	0.135	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	1.45163	client-A	1.0	0.851085	client-B	1.0	2.03544	client-C
2	0	False	20	2	1	3	1.0	0.17	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	3.70366	client-A	1.0	0.867765	client-B	1.0	1.31812	client-C
2	0	False	21	5	0	4	1.0	0.25	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	0.584166	client-A	0.0	0.684259	client-B	0.0	0.417359	client-C
2	0	False	22	4	0	2	1.0	0.135	0.0	1.0	1.50182	[0, 1, 2]	[0, 1, 2]	1.0	1.05125	client-A	1.0	0.917762	client-B	0.0	0.600834	client-C
2	0	False	23	4	0	2	1.0	0.135	1.0	0.0	3.1706	[0, 1, 2]	[0, 1, 2]	1.0	1.3682	client-A	0.0	0.767676	client-B	0.0	1.21802	client-C
2	0	False	24	5	0	3	1.0	0.17	2.0	0.0	7.64055	[0, 1, 2]	[0, 1, 2]	0.0	1.2014	client-A	0.0	0.728109	client-B	1.0	0.534137	client-C
2	0	False	25	0	1	2	1.0	0.135	-1.0	1.0	-1.0	[0, 1, 2]	[0, 1, 2]	1.0	2.01938	client-A	1.0	0.550761	client-B	1.0	1.46824	client-C
2	0	False	26	4	1	1	1.0	0.115	0.0	1.0	11.1103	[0, 1, 2]	[0, 1, 2]	0.0	1.7194	client-A	1.0	0.567476	client-B	1.0	0.951161	client-C
2	0	False	27	5	0	3	1.0	0.17	-1.0	0.0	-1.0	[0, 1, 2]	[0, 1, 2]	0.0	1.75184	client-A	0.0	0.834365	client-B	0.0	1.2514	client-C
2	0	False	28	4	0	2	1.0	0.135	1.0	1.0	19.057	[0, 1, 2]	[0, 1, 2]	0.0	1.31816	client-A	1.0	0.667564	client-B	0.0	0.567498	client-C
2	0	False	29	5	1	4	1.0	0.25	2.0	1.0	2.25243	[0, 1, 2]	[0, 1, 2]	1.0	0.984578	client-A	0.0	0.734273	client-B	1.0	0.567456	client-C
2	0	False	30	2	0	1	1.0	0.115	0.0	0.0	7.65724	[0, 1, 2]	[0, 1, 2]	0.0	2.30236	client-A	0.0	1.05124	client-B	1.0	1.25137	client-C
2	0	False	31	4	1	1	1.0	0.115	1.0	1.0	11.9673	[0, 1, 2]	[0, 1, 2]	1.0	2.10218	client-A	1.0	1.01789	client-B	0.0	0.884427	client-C
2	0	False	32	0	1	2	1.0	0.135	2.0	0.0	7.90735	[0, 1, 2]	[0, 1, 2]	0.0	3.28665	client-A	0.0	0.617172	client-B	1.0	1.21802	client-C
0	1	False	1	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.6184	client-B	0.0	0.700357	client-C	0.0	0.267202	client-A
0	1	False	2	1	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.61839	client-B	1.0	0.817696	client-C	1.0	0.817712	client-A
0	1	False	3	3	0	1	1.0	0.115	2.0	1.0	16.9487	[2, 0, 1]	[0, 1, 2]	1.0	0.734247	client-B	1.0	0.934424	client-C	0.0	0.667605	client-A
0	1	False	4	3	1	3	1.0	0.17	0.0	1.0	11.3106	[2, 0, 1]	[0, 1, 2]	1.0	1.15132	client-B	0.0	0.734287	client-C	1.0	1.11798	client-A
0	1	False	5	3	0	1	1.0	0.115	1.0	0.0	7.20674	[2, 0, 1]	[0, 1, 2]	1.0	1.2014	client-B	0.0	0.617478	client-C	0.0	0.717579	client-A
0	1	False	6	4	1	1	1.0	0.115	2.0	1.0	6.92325	[2, 0, 1]	[0, 1, 2]	1.0	3.68688	client-B	0.0	2.50252	client-C	1.0	4.85461	client-A
0	1	False	7	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	2.83614	client-B	0.0	0.467413	client-C	0.0	0.667587	client-A
0	1	False	8	5	0	3	1.0	0.17	0.0	0.0	11.027	[2, 0, 1]	[0, 1, 2]	1.0	0.800985	client-B	0.0	0.734264	client-C	0.0	0.667604	client-A
0	1	False	9	5	1	2	1.0	0.135	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	0.984476	client-B	1.0	0.617486	client-C	1.0	0.901123	client-A
0	1	False	10	5	1	2	1.0	0.135	1.0	1.0	2.48601	[2, 0, 1]	[0, 1, 2]	0.0	1.7018	client-B	1.0	0.500729	client-C	1.0	0.817713	client-A
0	1	False	11	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.00118	client-B	0.0	0.567405	client-C	0.0	0.250507	client-A
0	1	False	12	2	0	2	1.0	0.135	2.0	0.0	3.23668	[2, 0, 1]	[0, 1, 2]	1.0	0.985115	client-B	0.0	0.684215	client-C	0.0	0.534113	client-A
0	1	False	13	3	0	1	1.0	0.115	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.851056	client-B	0.0	0.63416	client-C	0.0	0.700947	client-A
0	1	False	14	3	0	1	1.0	0.115	0.0	1.0	1.90217	[2, 0, 1]	[0, 1, 2]	0.0	1.7185	client-B	1.0	0.600364	client-C	0.0	0.717624	client-A
0	1	False	15	5	0	3	1.0	0.17	1.0	0.0	8.17428	[2, 0, 1]	[0, 1, 2]	1.0	10.1426	client-B	0.0	0.76766	client-C	1.0	0.917795	client-A
0	1	False	16	4	1	1	1.0	0.115	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.10129	client-B	1.0	0.951254	client-C	1.0	0.750995	client-A
0	1	False	17	1	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.28476	client-B	1.0	0.917754	client-C	1.0	1.71849	client-A
0	1	False	18	5	1	2	1.0	0.135	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.51834	client-B	1.0	0.433964	client-C	1.0	0.667551	client-A
0	1	False	19	1	1	4	1.0	0.25	2.0	1.0	1.4351	[2, 0, 1]	[0, 1, 2]	0.0	1.28476	client-B	1.0	0.48408	client-C	1.0	0.68426	client-A
0	1	False	20	4	1	1	1.0	0.115	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.28474	client-B	1.0	0.617505	client-C	1.0	0.684266	client-A
0	1	False	21	5	0	3	1.0	0.17	0.0	0.0	1.28496	[2, 0, 1]	[0, 1, 2]	1.0	1.00117	client-B	0.0	0.60081	client-C	1.0	0.984529	client-A
0	1	False	22	2	0	2	1.0	0.135	1.0	0.0	2.25242	[2, 0, 1]	[0, 1, 2]	1.0	2.80281	client-B	0.0	0.68425	client-C	0.0	0.903058	client-A
0	1	False	23	3	1	3	1.0	0.17	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.06793	client-B	1.0	0.65088	client-C	1.0	0.734312	client-A
0	1	False	24	5	0	3	1.0	0.17	2.0	1.0	1.702	[2, 0, 1]	[0, 1, 2]	1.0	1.16797	client-B	0.0	0.700876	client-C	1.0	2.40243	client-A
0	1	False	25	2	0	2	1.0	0.135	0.0	0.0	1.59085	[2, 0, 1]	[0, 1, 2]	1.0	1.08464	client-B	0.0	0.534069	client-C	0.0	2.51922	client-A
0	1	False	26	5	1	2	1.0	0.135	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.65126	client-B	0.0	0.65088	client-C	0.0	1.20139	client-A
0	1	False	27	3	1	3	1.0	0.17	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.35151	client-B	1.0	0.60085	client-C	1.0	1.15136	client-A
0	1	False	28	2	0	2	1.0	0.135	1.0	1.0	9.5088	[2, 0, 1]	[0, 1, 2]	1.0	1.11795	client-B	0.0	0.884401	client-C	1.0	0.700942	client-A
0	1	False	29	3	1	3	1.0	0.17	2.0	0.0	5.30518	[2, 0, 1]	[0, 1, 2]	1.0	1.0846	client-B	0.0	0.767617	client-C	1.0	1.05212	client-A
0	1	False	30	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.684244	client-B	0.0	0.667592	client-C	0.0	0.217187	client-A
0	1	False	31	1	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.95201	client-B	1.0	0.60086	client-C	1.0	0.550814	client-A
0	1	False	32	4	1	1	1.0	0.115	0.0	1.0	5.70543	[2, 0, 1]	[0, 1, 2]	1.0	1.81857	client-B	0.0	2.96958	client-C	1.0	0.934489	client-A
1	1	False	1	3	1	1	1.0	0.115	1.0	1.0	7.30685	[2, 0, 1]	[0, 1, 2]	1.0	1.50157	client-B	0.0	0.850986	client-C	1.0	0.650878	client-A
1	1	False	2	3	1	1	1.0	0.115	2.0	0.0	22.954	[2, 0, 1]	[0, 1, 2]	0.0	1.28479	client-B	0.0	1.25137	client-C	1.0	0.801044	client-A
1	1	False	3	3	1	1	1.0	0.115	0.0	0.0	2.03557	[2, 0, 1]	[0, 1, 2]	1.0	0.917782	client-B	0.0	0.884376	client-C	0.0	0.4007	client-A
1	1	False	4	0	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.13463	client-B	1.0	0.817659	client-C	1.0	0.734302	client-A
1	1	False	5	3	1	1	1.0	0.115	1.0	1.0	4.98814	[2, 0, 1]	[0, 1, 2]	1.0	1.0012	client-B	1.0	0.801027	client-C	0.0	0.484095	client-A
1	1	False	6	3	0	2	1.0	0.135	2.0	0.0	0.617713	[2, 0, 1]	[0, 1, 2]	1.0	0.767677	client-B	0.0	0.867785	client-C	0.0	0.4516	client-A
1	1	False	7	0	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	4.88797	client-B	1.0	0.684265	client-C	1.0	1.63512	client-A
1	1	False	8	3	1	2	1.0	0.135	0.0	0.0	3.1922	[2, 0, 1]	[0, 1, 2]	1.0	1.30145	client-B	0.0	0.750986	client-C	1.0	0.784371	client-A
1	1	False	9	3	1	2	1.0	0.135	1.0	0.0	4.12078	[2, 0, 1]	[0, 1, 2]	0.0	1.13463	client-B	0.0	0.834307	client-C	1.0	0.66761	client-A
1	1	False	10	3	1	2	1.0	0.135	2.0	1.0	4.6769	[2, 0, 1]	[0, 1, 2]	1.0	2.25226	client-B	0.0	0.684193	client-C	1.0	0.567501	client-A
1	1	False	11	2	0	3	1.0	0.17	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.784312	client-B	0.0	0.517407	client-C	0.0	0.85108	client-A
1	1	False	12	3	0	1	1.0	0.115	0.0	0.0	1.23487	[2, 0, 1]	[0, 1, 2]	0.0	2.31904	client-B	1.0	2.01874	client-C	0.0	0.534135	client-A
1	1	False	13	2	1	3	1.0	0.17	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.10127	client-B	1.0	0.717573	client-C	1.0	1.25144	client-A
1	1	False	14	3	0	2	1.0	0.135	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.71849	client-B	0.0	0.617506	client-C	0.0	0.817717	client-A
1	1	False	15	0	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	0.800989	client-B	1.0	0.700911	client-C	1.0	0.550812	client-A
1	1	False	16	3	0	2	1.0	0.135	1.0	0.0	4.88809	[2, 0, 1]	[0, 1, 2]	0.0	0.749613	client-B	0.0	0.750917	client-C	1.0	1.05126	client-A
1	1	False	17	2	0	3	1.0	0.17	2.0	0.0	3.40409	[2, 0, 1]	[0, 1, 2]	1.0	1.1513	client-B	0.0	0.617509	client-C	0.0	0.617526	client-A
1	1	False	18	0	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.984501	client-B	0.0	0.6512	client-C	0.0	0.951131	client-A
1	1	False	19	3	1	2	1.0	0.135	0.0	0.0	3.37014	[2, 0, 1]	[0, 1, 2]	0.0	1.13462	client-B	0.0	0.73424	client-C	1.0	0.801034	client-A
1	1	False	20	3	0	1	1.0	0.115	1.0	1.0	4.08737	[2, 0, 1]	[0, 1, 2]	1.0	1.06794	client-B	0.0	0.684231	client-C	1.0	0.88447	client-A
1	1	False	21	0	0	4	1.0	0.25	2.0	0.0	4.58787	[2, 0, 1]	[0, 1, 2]	0.0	0.934487	client-B	0.0	0.584169	client-C	1.0	1.31816	client-A
1	1	False	22	2	1	3	1.0	0.17	0.0	1.0	0.851222	[2, 0, 1]	[0, 1, 2]	0.0	2.10214	client-B	1.0	1.03455	client-C	1.0	0.767675	client-A
1	1	False	23	2	0	3	1.0	0.17	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.851071	client-B	0.0	0.884372	client-C	0.0	0.417367	client-A
1	1	False	24	0	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.01786	client-B	0.0	0.500728	client-C	0.0	0.717644	client-A
1	1	False	25	0	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.08463	client-B	0.0	1.11799	client-C	0.0	0.534138	client-A
1	1	False	26	2	1	3	1.0	0.17	1.0	1.0	6.1225	[2, 0, 1]	[0, 1, 2]	0.0	1.06791	client-B	1.0	0.700944	client-C	0.0	0.717675	client-A
1	1	False	27	3	0	1	1.0	0.115	2.0	1.0	4.57115	[2, 0, 1]	[0, 1, 2]	0.0	2.03543	client-B	1.0	1.05118	client-C	1.0	1.31816	client-A
1	1	False	28	2	0	3	1.0	0.17	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.834375	client-B	0.0	0.734251	client-C	0.0	0.801009	client-A
1	1	False	29	0	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.00122	client-B	1.0	0.500768	client-C	1.0	0.584118	client-A
1	1	False	30	2	1	3	1.0	0.17	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	3.5034	client-B	1.0	0.817679	client-C	1.0	3.97048	client-A
1	1	False	31	3	0	2	1.0	0.135	0.0	1.0	1.11812	[2, 0, 1]	[0, 1, 2]	0.0	2.26896	client-B	1.0	1.00135	client-C	1.0	0.768304	client-A
1	1	False	32	3	0	1	1.0	0.115	1.0	0.0	4.58778	[2, 0, 1]	[0, 1, 2]	0.0	1.41817	client-B	0.0	1.0846	client-C	1.0	1.40157	client-A
2	1	False	1	1	0	2	1.0	0.135	2.0	0.0	4.27101	[2, 0, 1]	[0, 1, 2]	0.0	2.2022	client-B	0.0	1.06784	client-C	1.0	1.13463	client-A
2	1	False	2	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.28478	client-B	1.0	1.31812	client-C	1.0	0.584185	client-A
2	1	False	3	3	1	1	1.0	0.115	0.0	0.0	2.06898	[2, 0, 1]	[0, 1, 2]	1.0	1.18466	client-B	0.0	0.700916	client-C	0.0	0.884442	client-A
2	1	False	4	1	0	2	1.0	0.135	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.31815	client-B	0.0	1.05121	client-C	0.0	0.650976	client-A
2	1	False	5	3	0	1	1.0	0.115	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.88529	client-B	0.0	1.08459	client-C	0.0	0.650904	client-A
2	1	False	6	1	0	2	1.0	0.135	1.0	0.0	1.96887	[2, 0, 1]	[0, 1, 2]	1.0	1.45157	client-B	0.0	0.700917	client-C	0.0	0.634245	client-A
2	1	False	7	2	0	3	1.0	0.17	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.33479	client-B	0.0	0.95114	client-C	0.0	0.66762	client-A
2	1	False	8	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	0.667592	client-B	1.0	0.750985	client-C	1.0	0.567496	client-A
2	1	False	9	2	0	3	1.0	0.17	2.0	0.0	5.40527	[2, 0, 1]	[0, 1, 2]	0.0	0.934482	client-B	0.0	0.617466	client-C	1.0	0.851081	client-A
2	1	False	10	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.06793	client-B	1.0	0.667537	client-C	1.0	0.650911	client-A
2	1	False	11	1	0	2	1.0	0.135	0.0	0.0	1.70201	[2, 0, 1]	[0, 1, 2]	0.0	6.05568	client-B	1.0	0.917766	client-C	0.0	4.68778	client-A
2	1	False	12	3	0	1	1.0	0.115	1.0	1.0	8.85823	[2, 0, 1]	[0, 1, 2]	1.0	1.01786	client-B	0.0	1.10125	client-C	1.0	0.817731	client-A
2	1	False	13	2	0	3	1.0	0.17	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.38484	client-B	0.0	0.951116	client-C	0.0	0.50163	client-A
2	1	False	14	4	1	3	1.0	0.17	2.0	1.0	1.60199	[2, 0, 1]	[0, 1, 2]	0.0	1.28473	client-B	1.0	0.650816	client-C	1.0	0.650907	client-A
2	1	False	15	4	1	3	1.0	0.17	0.0	1.0	3.05322	[2, 0, 1]	[0, 1, 2]	1.0	1.21806	client-B	0.0	1.75181	client-C	1.0	6.53943	client-A
2	1	False	16	0	1	2	1.0	0.135	1.0	0.0	3.38706	[2, 0, 1]	[0, 1, 2]	0.0	2.15224	client-B	0.0	1.21768	client-C	1.0	0.767692	client-A
2	1	False	17	3	1	1	1.0	0.115	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.68515	client-B	1.0	0.750954	client-C	1.0	2.00206	client-A
2	1	False	18	0	1	2	1.0	0.135	2.0	1.0	12.3614	[2, 0, 1]	[0, 1, 2]	0.0	0.984524	client-B	0.0	0.801003	client-C	1.0	1.08494	client-A
2	1	False	19	5	0	4	1.0	0.25	0.0	0.0	1.33499	[2, 0, 1]	[0, 1, 2]	0.0	0.600858	client-B	0.0	0.600799	client-C	1.0	1.30148	client-A
2	1	False	20	5	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.767638	client-B	0.0	0.667565	client-C	0.0	0.233874	client-A
2	1	False	21	5	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.934447	client-B	0.0	0.617512	client-C	0.0	0.283895	client-A
2	1	False	22	3	0	1	1.0	0.115	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	11.5773	client-B	1.0	2.75268	client-C	1.0	0.68427	client-A
2	1	False	23	5	0	4	1.0	0.25	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	3.7036	client-B	0.0	0.717568	client-C	0.0	0.434032	client-A
2	1	False	24	0	1	2	1.0	0.135	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.71758	client-B	0.0	1.76854	client-C	0.0	0.81772	client-A
2	1	False	25	3	0	1	1.0	0.115	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.88531	client-B	1.0	0.684244	client-C	1.0	1.13468	client-A
2	1	False	26	3	1	1	1.0	0.115	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	1.41822	client-B	0.0	1.00117	client-C	0.0	0.667582	client-A
2	1	False	27	3	1	1	1.0	0.115	1.0	0.0	6.87313	[2, 0, 1]	[0, 1, 2]	1.0	1.00117	client-B	0.0	1.20132	client-C	1.0	2.23557	client-A
2	1	False	28	4	1	3	1.0	0.17	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	1.28474	client-B	1.0	0.95115	client-C	1.0	0.784369	client-A
2	1	False	29	4	1	3	1.0	0.17	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	2.30234	client-B	1.0	0.634176	client-C	1.0	0.667605	client-A
2	1	False	30	0	1	2	1.0	0.135	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	3.21986	client-B	1.0	0.78428	client-C	1.0	0.650902	client-A
2	1	False	31	2	0	3	1.0	0.17	-1.0	0.0	-1.0	[2, 0, 1]	[0, 1, 2]	0.0	0.817677	client-B	0.0	0.83433	client-C	0.0	1.08462	client-A
2	1	False	32	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[2, 0, 1]	[0, 1, 2]	1.0	0.734294	client-B	1.0	0.484	client-C	1.0	0.66755	client-A
0	2	False	1	2	0	2	1.0	0.135	1.0	0.0	4.08744	[1, 2, 0]	[0, 1, 2]	0.0	2.05214	client-C	0.0	0.70092	client-A	1.0	0.834371	client-B
0	2	False	2	3	0	1	1.0	0.115	2.0	1.0	3.13658	[1, 2, 0]	[0, 1, 2]	1.0	1.01784	client-C	0.0	0.667611	client-A	1.0	1.55167	client-B
0	2	False	3	0	1	3	1.0	0.17	0.0	0.0	1.18484	[1, 2, 0]	[0, 1, 2]	0.0	0.851002	client-C	0.0	0.567515	client-A	1.0	2.36916	client-B
0	2	False	4	2	0	2	1.0	0.135	1.0	1.0	5.87233	[1, 2, 0]	[0, 1, 2]	1.0	4.45387	client-C	0.0	0.634229	client-A	1.0	3.53672	client-B
0	2	False	5	3	0	3	1.0	0.17	2.0	0.0	2.58614	[1, 2, 0]	[0, 1, 2]	0.0	0.600831	client-C	1.0	0.801042	client-A	0.0	0.467389	client-B
0	2	False	6	0	1	3	1.0	0.17	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.86774	client-C	1.0	0.684275	client-A	1.0	0.600822	client-B
0	2	False	7	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	1.85189	client-C	0.0	0.534124	client-A	0.0	0.51744	client-B
0	2	False	8	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.617493	client-C	0.0	0.634228	client-A	0.0	0.333896	client-B
0	2	False	9	2	1	1	1.0	0.115	0.0	0.0	0.918576	[1, 2, 0]	[0, 1, 2]	0.0	0.901067	client-C	0.0	0.784371	client-A	1.0	1.35147	client-B
0	2	False	10	5	1	4	1.0	0.25	1.0	1.0	1.78541	[1, 2, 0]	[0, 1, 2]	0.0	5.87771	client-C	1.0	0.884465	client-A	1.0	0.734363	client-B
0	2	False	11	4	1	2	1.0	0.135	2.0	0.0	7.70734	[1, 2, 0]	[0, 1, 2]	1.0	1.15131	client-C	0.0	1.18471	client-A	0.0	2.05165	client-B
0	2	False	12	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.667545	client-C	0.0	0.517461	client-A	0.0	0.283891	client-B
0	2	False	13	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.717592	client-C	1.0	0.700972	client-A	1.0	0.5508	client-B
0	2	False	14	4	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.534092	client-C	0.0	0.567475	client-A	0.0	0.383952	client-B
0	2	False	15	3	0	3	1.0	0.17	0.0	1.0	5.75549	[1, 2, 0]	[0, 1, 2]	1.0	0.817662	client-C	1.0	0.684265	client-A	0.0	0.400636	client-B
0	2	False	16	0	1	3	1.0	0.17	1.0	1.0	2.38594	[1, 2, 0]	[0, 1, 2]	1.0	1.7351	client-C	0.0	8.94155	client-A	1.0	1.11667	client-B
0	2	False	17	2	1	1	1.0	0.115	2.0	0.0	3.93734	[1, 2, 0]	[0, 1, 2]	0.0	0.884383	client-C	1.0	1.3849	client-A	0.0	1.03454	client-B
0	2	False	18	0	1	3	1.0	0.17	0.0	0.0	5.5887	[1, 2, 0]	[0, 1, 2]	1.0	1.20135	client-C	0.0	1.13466	client-A	1.0	1.35151	client-B
0	2	False	19	4	1	2	1.0	0.135	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.83434	client-C	1.0	0.751005	client-A	1.0	2.01872	client-B
0	2	False	20	2	0	2	1.0	0.135	1.0	1.0	1.38505	[1, 2, 0]	[0, 1, 2]	0.0	1.15128	client-C	1.0	0.801102	client-A	1.0	0.817681	client-B
0	2	False	21	2	1	1	1.0	0.115	2.0	1.0	7.94079	[1, 2, 0]	[0, 1, 2]	1.0	3.79257	client-C	1.0	0.517534	client-A	0.0	5.77768	client-B
0	2	False	22	3	0	3	1.0	0.17	0.0	0.0	1.10141	[1, 2, 0]	[0, 1, 2]	1.0	2.4024	client-C	0.0	1.03458	client-A	0.0	1.80187	client-B
0	2	False	23	2	0	2	1.0	0.135	1.0	0.0	3.30342	[1, 2, 0]	[0, 1, 2]	1.0	1.76848	client-C	0.0	0.967325	client-A	0.0	0.800975	client-B
0	2	False	24	4	1	2	1.0	0.135	2.0	0.0	2.61948	[1, 2, 0]	[0, 1, 2]	0.0	1.11794	client-C	0.0	0.717628	client-A	1.0	0.750963	client-B
0	2	False	25	3	0	1	1.0	0.115	0.0	1.0	2.64226	[1, 2, 0]	[0, 1, 2]	1.0	0.817687	client-C	0.0	0.767614	client-A	1.0	0.584168	client-B
0	2	False	26	3	0	1	1.0	0.115	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	1.46829	client-C	0.0	1.41822	client-A	0.0	1.2848	client-B
0	2	False	27	3	0	1	1.0	0.115	1.0	1.0	3.7538	[1, 2, 0]	[0, 1, 2]	1.0	1.08461	client-C	1.0	0.634241	client-A	0.0	0.600836	client-B
0	2	False	28	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.98449	client-C	1.0	0.68427	client-A	1.0	0.700924	client-B
0	2	False	29	2	1	1	1.0	0.115	2.0	0.0	6.28938	[1, 2, 0]	[0, 1, 2]	0.0	3.01959	client-C	0.0	2.53675	client-A	1.0	1.95201	client-B
0	2	False	30	3	0	3	1.0	0.17	0.0	0.0	3.13652	[1, 2, 0]	[0, 1, 2]	1.0	0.801014	client-C	0.0	0.617545	client-A	0.0	0.951171	client-B
0	2	False	31	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.817932	client-C	1.0	0.651202	client-A	1.0	0.734892	client-B
0	2	False	32	4	1	2	1.0	0.135	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	1.21804	client-C	0.0	0.517477	client-A	0.0	0.684238	client-B
1	2	False	1	2	1	2	1.0	0.135	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.851044	client-C	1.0	0.684226	client-A	1.0	0.86769	client-B
1	2	False	2	4	0	1	1.0	0.115	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.53501	client-C	1.0	0.500763	client-A	1.0	0.700953	client-B
1	2	False	3	0	0	3	1.0	0.17	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.25138	client-C	1.0	0.834412	client-A	1.0	1.51832	client-B
1	2	False	4	1	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.16795	client-C	1.0	0.500784	client-A	1.0	0.667567	client-B
1	2	False	5	2	0	2	1.0	0.135	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	2.01871	client-C	1.0	0.517441	client-A	1.0	1.08459	client-B
1	2	False	6	2	1	1	1.0	0.115	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.851055	client-C	1.0	0.567566	client-A	1.0	0.851062	client-B
1	2	False	7	1	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.834379	client-C	1.0	0.517454	client-A	1.0	0.667582	client-B
1	2	False	8	2	0	2	1.0	0.135	1.0	1.0	2.80299	[1, 2, 0]	[0, 1, 2]	1.0	1.18468	client-C	1.0	0.600869	client-A	0.0	1.58505	client-B
1	2	False	9	4	0	1	1.0	0.115	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.11791	client-C	1.0	0.851083	client-A	1.0	0.650846	client-B
1	2	False	10	2	1	1	1.0	0.115	2.0	0.0	4.52116	[1, 2, 0]	[0, 1, 2]	0.0	1.25143	client-C	1.0	0.617546	client-A	0.0	0.90111	client-B
1	2	False	11	2	1	1	1.0	0.115	0.0	1.0	4.93814	[1, 2, 0]	[0, 1, 2]	1.0	3.0029	client-C	0.0	1.53502	client-A	1.0	0.667516	client-B
1	2	False	12	2	0	2	1.0	0.135	1.0	1.0	11.5273	[1, 2, 0]	[0, 1, 2]	1.0	0.734269	client-C	0.0	0.517	client-A	1.0	0.700927	client-B
1	2	False	13	1	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.43491	client-C	1.0	0.750993	client-A	1.0	0.55083	client-B
1	2	False	14	2	1	2	1.0	0.135	2.0	1.0	4.77694	[1, 2, 0]	[0, 1, 2]	1.0	0.800966	client-C	1.0	0.73438	client-A	0.0	0.484046	client-B
1	2	False	15	4	1	3	1.0	0.17	0.0	1.0	1.06813	[1, 2, 0]	[0, 1, 2]	1.0	1.33478	client-C	0.0	0.550772	client-A	1.0	0.733796	client-B
1	2	False	16	0	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.967803	client-C	0.0	0.550815	client-A	0.0	0.600774	client-B
1	2	False	17	0	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.667559	client-C	0.0	0.567516	client-A	0.0	0.60085	client-B
1	2	False	18	1	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.867697	client-C	1.0	0.667584	client-A	1.0	0.53407	client-B
1	2	False	19	2	1	2	1.0	0.135	1.0	0.0	7.75731	[1, 2, 0]	[0, 1, 2]	1.0	2.66926	client-C	0.0	1.68516	client-A	1.0	1.25145	client-B
1	2	False	20	0	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.851077	client-C	0.0	0.951117	client-A	0.0	0.467354	client-B
1	2	False	21	0	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.534107	client-C	0.0	0.617542	client-A	0.0	0.300567	client-B
1	2	False	22	0	0	3	1.0	0.17	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	1.48495	client-C	0.0	0.667634	client-A	0.0	0.694774	client-B
1	2	False	23	4	1	3	1.0	0.17	2.0	1.0	1.18488	[1, 2, 0]	[0, 1, 2]	1.0	3.58682	client-C	1.0	1.40158	client-A	0.0	0.851089	client-B
1	2	False	24	2	1	1	1.0	0.115	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.90193	client-C	1.0	0.967849	client-A	1.0	2.71937	client-B
1	2	False	25	0	0	3	1.0	0.17	0.0	0.0	1.55185	[1, 2, 0]	[0, 1, 2]	0.0	1.00117	client-C	0.0	0.73431	client-A	1.0	0.133746	client-B
1	2	False	26	4	1	3	1.0	0.17	1.0	1.0	2.46936	[1, 2, 0]	[0, 1, 2]	1.0	2.56921	client-C	1.0	0.734337	client-A	0.0	0.734294	client-B
1	2	False	27	2	0	2	1.0	0.135	2.0	1.0	1.81873	[1, 2, 0]	[0, 1, 2]	0.0	1.08463	client-C	1.0	0.817721	client-A	1.0	0.750975	client-B
1	2	False	28	2	1	2	1.0	0.135	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.30144	client-C	1.0	0.751024	client-A	1.0	1.93597	client-B
1	2	False	29	0	0	3	1.0	0.17	0.0	0.0	1.18481	[1, 2, 0]	[0, 1, 2]	0.0	0.484022	client-C	1.0	0.834403	client-A	0.0	1.48445	client-B
1	2	False	30	4	0	1	1.0	0.115	1.0	1.0	3.52027	[1, 2, 0]	[0, 1, 2]	1.0	1.01786	client-C	0.0	0.801015	client-A	1.0	1.41822	client-B
1	2	False	31	4	1	3	1.0	0.17	2.0	1.0	2.00225	[1, 2, 0]	[0, 1, 2]	0.0	1.33478	client-C	1.0	0.684271	client-A	1.0	0.767672	client-B
1	2	False	32	4	0	1	1.0	0.115	0.0	1.0	1.41838	[1, 2, 0]	[0, 1, 2]	1.0	0.767616	client-C	1.0	0.734312	client-A	0.0	0.534142	client-B
2	2	False	1	0	1	2	1.0	0.135	1.0	1.0	2.60281	[1, 2, 0]	[0, 1, 2]	1.0	0.967778	client-C	0.0	1.46826	client-A	1.0	1.01784	client-B
2	2	False	2	2	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.767625	client-C	0.0	0.600851	client-A	0.0	0.250513	client-B
2	2	False	3	2	0	3	1.0	0.17	2.0	1.0	5.1217	[1, 2, 0]	[0, 1, 2]	1.0	0.817663	client-C	1.0	1.68515	client-A	0.0	12.3002	client-B
2	2	False	4	0	1	2	1.0	0.135	0.0	1.0	0.917934	[1, 2, 0]	[0, 1, 2]	1.0	0.784321	client-C	0.0	0.617545	client-A	1.0	1.35149	client-B
2	2	False	5	0	0	1	1.0	0.115	1.0	1.0	3.03653	[1, 2, 0]	[0, 1, 2]	1.0	1.08456	client-C	0.0	0.600957	client-A	1.0	0.684227	client-B
2	2	False	6	0	0	2	1.0	0.135	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.901097	client-C	0.0	1.7185	client-A	0.0	0.567477	client-B
2	2	False	7	0	0	2	1.0	0.135	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.70179	client-C	1.0	0.717628	client-A	1.0	0.934468	client-B
2	2	False	8	2	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.434026	client-C	0.0	0.617542	client-A	0.0	0.283891	client-B
2	2	False	9	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	5.77753	client-C	1.0	0.584182	client-A	1.0	0.634217	client-B
2	2	False	10	0	0	1	1.0	0.115	2.0	0.0	4.98825	[1, 2, 0]	[0, 1, 2]	0.0	0.0336674	client-C	0.0	0.750975	client-A	1.0	0.683905	client-B
2	2	False	11	1	1	1	1.0	0.115	0.0	1.0	1.03475	[1, 2, 0]	[0, 1, 2]	1.0	1.08455	client-C	0.0	0.700957	client-A	1.0	0.734257	client-B
2	2	False	12	2	0	3	1.0	0.17	1.0	0.0	2.30253	[1, 2, 0]	[0, 1, 2]	1.0	0.951106	client-C	0.0	0.667591	client-A	0.0	0.550796	client-B
2	2	False	13	2	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.600808	client-C	0.0	0.61754	client-A	0.0	0.434008	client-B
2	2	False	14	0	0	1	1.0	0.115	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.38487	client-C	1.0	0.617554	client-A	1.0	1.06791	client-B
2	2	False	15	0	1	3	1.0	0.17	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.05123	client-C	1.0	1.41825	client-A	1.0	1.0679	client-B
2	2	False	16	1	1	1	1.0	0.115	2.0	1.0	3.42019	[1, 2, 0]	[0, 1, 2]	0.0	0.968425	client-C	1.0	0.817729	client-A	1.0	3.52069	client-B
2	2	False	17	2	0	3	1.0	0.17	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.48405	client-C	0.0	0.517475	client-A	0.0	0.500739	client-B
2	2	False	18	0	0	2	1.0	0.135	0.0	1.0	1.23488	[1, 2, 0]	[0, 1, 2]	1.0	0.917746	client-C	0.0	5.5225	client-A	1.0	1.61841	client-B
2	2	False	19	2	0	3	1.0	0.17	1.0	0.0	1.61862	[1, 2, 0]	[0, 1, 2]	1.0	0.98449	client-C	0.0	0.768545	client-A	0.0	0.550819	client-B
2	2	False	20	0	0	1	1.0	0.115	2.0	1.0	2.6862	[1, 2, 0]	[0, 1, 2]	0.0	3.03631	client-C	1.0	3.82037	client-A	1.0	4.18735	client-B
2	2	False	21	0	1	2	1.0	0.135	0.0	1.0	0.923586	[1, 2, 0]	[0, 1, 2]	1.0	0.91778	client-C	0.0	0.917805	client-A	1.0	2.61927	client-B
2	2	False	22	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.15128	client-C	1.0	0.750997	client-A	1.0	0.650396	client-B
2	2	False	23	1	1	1	1.0	0.115	1.0	1.0	1.36836	[1, 2, 0]	[0, 1, 2]	0.0	0.517392	client-C	1.0	0.98453	client-A	1.0	0.617493	client-B
2	2	False	24	2	0	4	1.0	0.25	-1.0	0.0	-1.0	[1, 2, 0]	[0, 1, 2]	0.0	0.634152	client-C	0.0	0.600844	client-A	0.0	0.333918	client-B
2	2	False	25	0	1	3	1.0	0.17	2.0	1.0	0.684418	[1, 2, 0]	[0, 1, 2]	1.0	1.35148	client-C	0.0	1.7352	client-A	1.0	1.03453	client-B
2	2	False	26	1	1	1	1.0	0.115	0.0	1.0	5.30511	[1, 2, 0]	[0, 1, 2]	1.0	1.03451	client-C	0.0	0.700953	client-A	1.0	0.800972	client-B
2	2	False	27	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.06793	client-C	1.0	0.751076	client-A	1.0	0.500734	client-B
2	2	False	28	5	1	4	1.0	0.25	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	0.834379	client-C	1.0	0.550815	client-A	1.0	0.484048	client-B
2	2	False	29	0	1	3	1.0	0.17	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	1.30144	client-C	1.0	1.95206	client-A	1.0	1.53498	client-B
2	2	False	30	0	1	2	1.0	0.135	-1.0	1.0	-1.0	[1, 2, 0]	[0, 1, 2]	1.0	3.21981	client-C	1.0	0.550898	client-A	1.0	0.767644	client-B
2	2	False	31	0	1	3	1.0	0.17	1.0	1.0	2.28585	[1, 2, 0]	[0, 1, 2]	1.0	0.667559	client-C	0.0	0.534123	client-A	1.0	0.734287	client-B
2	2	False	32	0	0	2	1.0	0.135	2.0	1.0	1.98556	[1, 2, 0]	[0, 1, 2]	0.0	1.68506	client-C	1.0	1.61841	client-A	1.0	1.38486	client-B
